package com.mitocode.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Rol;

public interface IRolRepo extends JpaRepository<Rol, Integer>{

	@Query(value ="select r.* from usuario u\r\n" + 
			" inner join usuario_rol ur on ur.id_usuario=u.id_usuario\r\n" + 
			" inner join rol r on r.id_rol=ur.id_rol\r\n" + 
			" where u.nombre = :nombre ;", nativeQuery = true)
	List<Object[]> listarRolPorUsuario(@Param("nombre") String nombre);
}
