package com.mitocode.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.model.Rol;
import com.mitocode.repo.IRolRepo;
import com.mitocode.service.IRolService;

@Service
public class RolServiceImpl implements IRolService{
	@Autowired
	private IRolRepo repo;

	@Override
	public Rol registrar(Rol rol) {
		return repo.save(rol);
	}

	@Override
	public Rol modificar(Rol rol) {
		return repo.save(rol);
	}

	@Override
	public void eliminar(Integer idRol) {
		repo.deleteById(idRol);
	}

	@Override
	public Rol listarPorId(Integer idRol) {
		Optional<Rol> op = repo.findById(idRol);
		return op.isPresent() ? op.get() : new Rol();
	}

	@Override
	public List<Rol> listar() {
		return repo.findAll();
	}

	@Override
	public List<Rol> listarRolPorUsuario(String nombre) {		
		List<Rol> roles = new ArrayList<>();
		repo.listarRolPorUsuario(nombre).forEach( x -> {
			Rol r = new Rol();
			r.setIdRol((Integer.parseInt(String.valueOf(x[0]))));
			r.setDescripcion(String.valueOf(x[1]));
			r.setNombre(String.valueOf(x[2]));
	
			roles.add(r);
		});
		return roles;			
	}

}
