package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mitocode.model.Cliente;
import com.mitocode.repo.IClienteRepo;
import com.mitocode.service.IClienteService;

@Service
public class ClienteServiceImpl implements IClienteService{

	@Autowired
	private IClienteRepo repo;
	
	
	@Override
	public Cliente registrar(Cliente c) {
		c.getUsuario().setCliente(c);
		return repo.save(c);
	}

	@Transactional
	@Override
	public Cliente modificar(Cliente c) {
		if(c.getFoto().length > 0) {
			repo.modificarFoto(c.getIdCliente(), c.getFoto());			
		}		
		return repo.save(c);
	}

	@Override
	public List<Cliente> listar() {
		return repo.findAll();
	}

	@Override
	public Cliente listarPorId(Integer id) { 
		Optional<Cliente> op = repo.findById(id);
		return op.isPresent() ? op.get() : new Cliente();
	}

	@Override
	public void eliminar(Integer id) {
		repo.deleteById(id);
	}

	@Override
	public Page<Cliente> listarPageable(Pageable pageable) {
		return repo.findAll(pageable);
	}

	@Override
	public Cliente listarClientePorUsuario(String nombre) {
		return repo.listarClientePorUsuario(nombre);
	}

}
